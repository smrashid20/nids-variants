import os
import pickle
import random

import numpy as np
import torch
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.tree import DecisionTreeClassifier

from nids_blackbox_data_generator import get_training_data, dataset_test

debug = False
method = "dt"
label_ratio = 0.1

np.random.seed(12345)
torch.manual_seed(12345)

random.seed(12345)

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

total_dataset, labeled_dataset, unlabeled_dataset = get_training_data(label_ratio=label_ratio)
cat_dict = total_dataset.get_cat_dict()
print(cat_dict)
test_dataset = dataset_test(cat_dict)

num_data = total_dataset.get_x()
labels = total_dataset.get_y()

total_original_label_counts = dict()
distinct_labels, distinct_label_counts = np.unique(labels, return_counts=True)

for i in range(len(distinct_labels)):
    if distinct_labels[i] != -1:
        total_original_label_counts[distinct_labels[i]] = distinct_label_counts[i]

'''
undercomplete autoencoder for embedding.
'''


def tree_work():
    dt_X = labeled_dataset.get_x()
    dt_Y = labeled_dataset.get_y()

    print("labeled members dimensions:")
    print(dt_X.shape)
    print(dt_Y.shape)

    clf = DecisionTreeClassifier(random_state=0)
    clf.fit(dt_X, dt_Y)

    print("No. of leaves of decision tree:")
    print(clf.get_n_leaves())

    '''
    saving decision tree, cluster membership info (this is just to skip the clustering step for our faster use) and soft
    labeling info.
    '''

    file = open(os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio), 'tree.pkl'), 'wb')
    pickle.dump(clf, file)
    file.close()

    leaf_dataset_X_ = dict()
    leaf_dataset_Y_ = dict()

    '''
    finding out corresponding leaf for each training sample.
    '''

    for j in range(len(num_data)):
        leaf = clf.apply([num_data[j]])[0]
        if labels[j] != -1:
            leaf_dataset_X_.setdefault(leaf, []).append(num_data[j])
            leaf_dataset_Y_.setdefault(leaf, []).append(labels[j])

    for k, v in leaf_dataset_X_.items():
        leaf_dataset_X_[k] = np.array(leaf_dataset_X_[k])
        leaf_dataset_Y_[k] = np.array(leaf_dataset_Y_[k])

    return leaf_dataset_X_, leaf_dataset_Y_


def generate_result(test_X, label_ratio_, test_Y=None, path_to_model='models'):
    clf = pickle.load(
        file=open(os.path.join(path_to_model, 'models_' + str(method) + "_" + str(label_ratio_), 'tree.pkl'), 'rb'))

    # print("Predicting Leaf nodes...")
    test_Y_pred = clf.predict(test_X)

    if test_Y is not None:
        print(confusion_matrix(test_Y, test_Y_pred))
        print(classification_report(test_Y, test_Y_pred))

    return test_Y_pred


def train_model():
    if not os.path.exists('models'):
        os.mkdir('models')

    if not os.path.exists(os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio))):
        os.mkdir(os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio)))

    tree_work()


#
# #train_model()
# generate_result(test_dataset.get_x(), test_Y=test_dataset.get_y(), label_ratio_=label_ratio)
