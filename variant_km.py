import os
import pickle

import numpy as np
import torch
import random
from sklearn.cluster import KMeans
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.metrics import pairwise_distances_argmin_min, pairwise_distances

from nids_blackbox_data_generator import get_training_data, dataset_test

debug = False
method = 'km'
label_ratio = 1.0

np.random.seed(12345)
torch.manual_seed(12345)

random.seed(12345)

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

total_dataset, labeled_dataset, unlabeled_dataset = get_training_data(label_ratio=label_ratio)
cat_dict = total_dataset.get_cat_dict()
test_dataset = dataset_test(cat_dict)

num_data = total_dataset.get_x()
labels = total_dataset.get_y()

total_original_label_counts = dict()
distinct_labels, distinct_label_counts = np.unique(labels, return_counts=True)

for i in range(len(distinct_labels)):
    if distinct_labels[i] != -1:
        total_original_label_counts[distinct_labels[i]] = distinct_label_counts[i]


def tree_work(load_cluster_from_file=False):
    if load_cluster_from_file:
        clustering = pickle.load(
            file=open(os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio), 'clustering.pkl'), 'rb'))
    else:
        clustering = KMeans(n_clusters=int(total_dataset.__len__() / 85), random_state=0)
        print("Clustering Started.")
        clustering.fit(num_data)
        print("Clustering ended.")

    all_cluster_centers = clustering.cluster_centers_
    closest, _ = pairwise_distances_argmin_min(num_data, all_cluster_centers)

    print(closest.shape)

    cluster_to_labels_dict = dict()
    for j in range(len(closest)):
        if labels[j] >= 0:
            cluster_to_labels_dict.setdefault(closest[j], []).append(labels[j])

    cluster_to_label_dict = dict()
    for k, v in cluster_to_labels_dict.items():
        un, cnt = np.unique(v, return_counts=True)
        un_idx = np.argmax(cnt)
        cluster_to_label_dict[k] = un[un_idx]

    print(cluster_to_label_dict)

    file = open(os.path.join('models', 'models_' +
                             str(method) + "_" + str(label_ratio), 'cluster_to_label_dict.pkl'), 'wb')
    pickle.dump(cluster_to_label_dict, file)
    file.close()

    file = open(os.path.join('models', 'models_' +
                             str(method) + "_" + str(label_ratio), 'all_cluster_centers.pkl'), 'wb')
    pickle.dump(all_cluster_centers, file)
    file.close()

    if not load_cluster_from_file:
        file = open(os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio), 'clustering.pkl'), 'wb')
        pickle.dump(clustering, file)
        file.close()


def generate_result(test_X, label_ratio_, test_Y=None, path_to_model='models'):
    cluster_to_label_dict = pickle.load(
        file=open(os.path.join(path_to_model, 'models_' + str(method) + "_" +
                               str(label_ratio_), 'cluster_to_label_dict.pkl'), 'rb'))

    cluster_to_label_dict = dict(cluster_to_label_dict)
    all_cluster_centers = pickle.load(
        file=open(os.path.join(path_to_model, 'models_' + str(method) + "_" +
                               str(label_ratio_), 'all_cluster_centers.pkl'), 'rb'))

    pd = pairwise_distances(X=test_X, Y=all_cluster_centers)
    pd_s = np.argsort(pd, axis=1)
    #print(pd_s.shape)

    test_Y_pred = np.zeros(test_X.shape[0])
    for i in range(len(pd_s)):
        for j in range(len(pd_s[i])):
            if pd_s[i][j] in cluster_to_label_dict.keys():
                test_Y_pred[i] = cluster_to_label_dict[pd_s[i][j]]
                break

    test_Y_pred = np.array(test_Y_pred)

    if test_Y is not None:
        print(confusion_matrix(test_Y, test_Y_pred))
        print(classification_report(test_Y, test_Y_pred))

    return test_Y_pred


def train_model():
    if not os.path.exists('models'):
        os.mkdir('models')

    if not os.path.exists(os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio))):
        os.mkdir(os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio)))

    tree_work(load_cluster_from_file=True)