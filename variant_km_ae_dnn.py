import os
import pickle

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import random
from sklearn.cluster import KMeans
from sklearn.metrics import confusion_matrix, classification_report
from torch.nn import Linear
from torch.utils.data import DataLoader

from nids_blackbox_data_generator import get_training_data, dataset_test

debug = False
method = "km_ae_dnn"
label_ratio = 0.5

np.random.seed(12345)
torch.manual_seed(12345)

random.seed(12345)

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

total_dataset, labeled_dataset, unlabeled_dataset = get_training_data(label_ratio=label_ratio)
cat_dict = total_dataset.get_cat_dict()
print(cat_dict)
test_dataset = dataset_test(cat_dict)

ae_epoch = 80
pretrain_epoch = 200

num_data = total_dataset.get_x()
labels = total_dataset.get_y()

total_original_label_counts = dict()
distinct_labels, distinct_label_counts = np.unique(labels, return_counts=True)

for i in range(len(distinct_labels)):
    if distinct_labels[i] != -1:
        total_original_label_counts[distinct_labels[i]] = distinct_label_counts[i]

'''
undercomplete autoencoder for embedding.
'''


class AE(nn.Module):

    def __init__(self, n_input, n_z):
        super(AE, self).__init__()
        # encoder
        self.enc_1 = Linear(n_input, 96)
        self.enc_2 = Linear(96, 64)
        self.z_layer = Linear(64, n_z)

        # decoder
        self.dec_1 = Linear(n_z, 64)
        self.dec_2 = Linear(64, 96)
        self.x_bar_layer = Linear(96, n_input)

    def forward(self, x):
        # encoder
        enc_h1 = F.relu(self.enc_1(x))
        enc_h2 = F.relu(self.enc_2(enc_h1))
        z = self.z_layer(enc_h2)

        # decoder
        dec_h1 = F.relu(self.dec_1(z))
        dec_h2 = F.relu(self.dec_2(dec_h1))
        x_bar = self.x_bar_layer(dec_h2)

        return x_bar, z


class leaf_dnn(nn.Module):

    def __init__(self, n_input, n_output):
        super(leaf_dnn, self).__init__()
        self.fc1 = nn.Linear(n_input, 16)
        self.fc2 = nn.Linear(16, 8)
        self.fc3 = nn.Linear(8, n_output)

    def forward(self, x):
        out_1 = torch.relu(self.fc1(x))
        out_2 = torch.relu(self.fc2(out_1))
        out_3 = self.fc3(out_2)

        return out_3


def tree_work(load_cluster_from_file=False):
    if load_cluster_from_file:
        clustering = pickle.load(
            file=open(os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio), 'clustering.pkl'), 'rb'))
    else:
        clustering = KMeans(n_clusters=int(total_dataset.__len__() / 85), random_state=0)
        print("Clustering Started.")
        clustering.fit(num_data)
        print("Clustering ended.")

    cluster_assignment = clustering.labels_
    all_clusters = dict()
    for j in range(len(cluster_assignment)):
        all_clusters.setdefault(cluster_assignment[j], []).append(num_data[j])

    cluster_to_label_dict = dict()
    for j in range(len(cluster_assignment)):
        if labels[j] != -1:
            cluster_to_label_dict.setdefault(cluster_assignment[j], []).append(labels[j])

    # print("Clusters:")
    label_to_cluster_dict = dict()
    for k, v in cluster_to_label_dict.items():
        cl_labels, cl_label_counts = np.unique(np.array(v), return_counts=True)  # labeled member count in each cluster

        total_labeled_counts = np.sum(cl_label_counts)
        max_label = np.argmax(cl_label_counts)

        '''
        If a cluster contains more than 10% of all samples of a label present in the training dataset, it is considered 
        important for this label.
        If the majority label is not the normal label and there are no other labels for which this cluster is important,
        having more than 50% of the labeled members would be enough for soft labeling
        If the majority label is the normal label, then all labeled members must be normal for soft labeling.
        In any other case, we do not soft label.
        '''

        imp_for_label = []
        for label, total_label_count in total_original_label_counts.items():
            for j in range(len(cl_labels)):
                if cl_labels[j] == label and cl_label_counts[j] > 0.1 * total_label_count:
                    imp_for_label.append(label)

        if (cl_label_counts[max_label] / total_labeled_counts) > 0.5:
            selected_label = cl_labels[max_label]
            if len(imp_for_label) == 1:
                if imp_for_label[0] == selected_label:
                    if selected_label != int(cat_dict['Normal']):
                        label = selected_label
                        size = len(v)
                        label_to_cluster_dict.setdefault(label, []).append([k, size])
                    else:
                        if len(cl_labels) == 1:
                            label = selected_label
                            size = len(v)
                            label_to_cluster_dict.setdefault(label, []).append([k, size])
            elif len(imp_for_label) == 0:
                if selected_label != int(cat_dict['Normal']):
                    label = selected_label
                    size = len(v)
                    label_to_cluster_dict.setdefault(label, []).append([k, size])
                else:
                    if len(cl_labels) == 1:
                        label = selected_label
                        size = len(v)
                        label_to_cluster_dict.setdefault(label, []).append([k, size])

    '''
    clusters that belong to a particular label, after soft labeling.
    '''

    soft_label_mapping = dict()
    for k, v in label_to_cluster_dict.items():
        for cluster_index in v:
            soft_label_mapping[cluster_index[0]] = k

    '''
    soft labeling particular unlabeled samples.
    Also add this to labeled dataset.
    '''

    new_samples = []
    new_samples_labels = []
    for j in range(len(labels)):
        if labels[j] == -1 and (int(cluster_assignment[j]) in soft_label_mapping.keys()):
            labels[j] = soft_label_mapping[cluster_assignment[j]]
            new_samples.append(num_data[j])
            new_samples_labels.append(labels[j])

    labeled_dataset.add_sample(np.array(new_samples), np.array(new_samples_labels))

    '''
    checking total labeled and soft labeled members.
    '''

    total_soft_label_counts = dict()
    distinct_slabels, distinct_slabel_counts = np.unique(labels, return_counts=True)
    for j in range(len(distinct_slabels)):
        if distinct_slabels[j] != -1:
            total_soft_label_counts[distinct_slabels[j]] = distinct_slabel_counts[j]

    print("Label Count After Soft Labeling: ")
    print(total_soft_label_counts)

    cluster_to_labels_dict = dict()
    for k, v in cluster_to_label_dict.items():
        cl_labels = np.unique(np.array(v))
        cl_labels = sorted(list(cl_labels))
        if cl_labels[0] == -1:
            cl_labels = cl_labels[1:]
        cluster_to_labels_dict[k] = cl_labels

    total_dataset.set_y(labels)

    ############# suspicious #####################

    for k in list(all_clusters.keys()):
        if int(k) not in soft_label_mapping.keys():
            soft_label_mapping[int(k)] = int(cat_dict['Normal'])

    ###############################################

    file = open(os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio), 'soft_label_mapping.pkl'),
                'wb')
    pickle.dump(soft_label_mapping, file)
    file.close()

    if not load_cluster_from_file:
        file = open(os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio), 'clustering.pkl'), 'wb')
        pickle.dump(clustering, file)
        file.close()


def train_ae(epochs, load_from_file=False,
             save_path=os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio), 'train_ae')):
    '''
    train autoencoder
    '''

    model = AE(total_dataset.get_feature_shape(), 32)
    model.to(device)

    if load_from_file:
        model.load_state_dict(torch.load(save_path))

    else:
        ae_train_ds = total_dataset
        training_data_length = int(0.7 * ae_train_ds.__len__())
        validation_data_length = ae_train_ds.__len__() - training_data_length
        training_data, validation_data = torch.utils.data.random_split(ae_train_ds,
                                                                       [training_data_length, validation_data_length])

        train_loader = DataLoader(training_data, batch_size=32, shuffle=True)
        validation_loader = DataLoader(validation_data, batch_size=32, shuffle=True)

        optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
        min_val_loss = 1000000

        for epoch in range(epochs):
            training_loss = 0.
            validation_loss = 0.
            train_batch_num = 0
            val_batch_num = 0

            model.train()
            for batch_idx, (x, _, idx) in enumerate(train_loader):
                x = x.float()
                x = x.to(device)

                train_batch_num = batch_idx

                optimizer.zero_grad()
                x_bar, z = model(x)
                loss = F.mse_loss(x_bar, x)
                training_loss += loss.item()

                loss.backward()
                optimizer.step()

            training_loss /= (train_batch_num + 1)

            model.eval()

            for batch_idx, (x, _, idx) in enumerate(validation_loader):
                x = x.float()
                x = x.to(device)

                val_batch_num = batch_idx

                x_bar, z = model(x)
                loss = F.mse_loss(x_bar, x)
                validation_loss += loss.item()

            validation_loss /= (val_batch_num + 1)

            if epoch % 1 == 0:
                print(
                    "epoch {} , Training loss={:.4f}, Validation loss={:.4f}".format(epoch, training_loss,
                                                                                     validation_loss))

            if epoch == 0 or min_val_loss > validation_loss:
                min_val_loss = validation_loss
                torch.save(model.state_dict(), save_path)

        print("model saved to {}.".format(save_path))
    return model


'''
pretraining using labeled and soft labeled dataset.
'''


def pretrain_leaf_dnn(save_path, epochs):
    ae_model = AE(total_dataset.get_feature_shape(), 32)
    ae_model.load_state_dict(
        torch.load(os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio), 'train_ae')))
    ae_model.to(device)

    model = leaf_dnn(32, int(max(labels)) + 1)
    model.to(device)

    weights = torch.FloatTensor(labeled_dataset.get_weight()).to(device)
    train_loader = DataLoader(labeled_dataset, batch_size=32, shuffle=True)  # soft label must be assigned

    optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
    min_train_loss = 1000000

    for epoch in range(epochs):
        train_loss = 0.0
        train_batch_num = 0
        train_num_correct = 0
        train_num_examples = 0

        model.train()
        for batch_idx, (x, y_t, idx) in enumerate(train_loader):
            x = x.float()
            x = x.to(device)
            train_batch_num = batch_idx

            optimizer.zero_grad()

            x_emb = ae_model(x)[1]
            y_pred = model(x_emb)

            y_t = y_t.clone().detach().to(device)

            loss = torch.nn.CrossEntropyLoss(weight=weights)(y_pred, y_t)
            train_loss += loss.item()

            loss.backward()
            optimizer.step()

            correct = torch.eq(torch.max(torch.softmax(y_pred, dim=-1), dim=1)[1], y_t).view(-1)
            train_num_correct += torch.sum(correct).item()
            train_num_examples += correct.shape[0]

        train_loss /= (train_batch_num + 1)
        train_acc = train_num_correct / train_num_examples

        if epoch % 1 == 0:
            print("epoch {}; T loss={:.4f} T Accuracy={:.4f}".
                  format(epoch, train_loss, train_num_correct / train_num_examples))

        if epoch == 0 or min_train_loss > train_loss:
            min_train_loss = train_loss
            torch.save(model.state_dict(), save_path)

    print("model saved to {}.".format(save_path))

    return model


'''
The dictionary of X-Y training dataset for each individual leaf.
'''
'''
training dataset of an individual leaf.
'''


def generate_result(test_X, label_ratio_, test_Y=None, path_to_model='models'):
    # clustering = pickle.load(file=open(os.path.join(path_to_model, 'models_' + str(method) + "_" + str(label_ratio_),
    #                                                 'clustering.pkl'), 'rb'))
    # soft_label_mapping = pickle.load(
    #     file=open(os.path.join(path_to_model, 'models_' + str(method) + "_" + str(label_ratio_),
    #                            'soft_label_mapping.pkl'), 'rb'))
    #
    # print("Assigning Cluster...")
    # cluster_assignment = clustering.predict(test_X)
    # print("Cluster Assignment Done.")

    ae_model = AE(total_dataset.get_feature_shape(), 32)
    ae_model.load_state_dict(torch.load(os.path.join(path_to_model, 'models_' + str(method) + "_" + str(label_ratio_),
                                                     'train_ae')))
    ae_model.to(device)

    dnn_model = leaf_dnn(32, int(max(labels)) + 1)
    dnn_model.load_state_dict(torch.load(os.path.join(path_to_model, 'models_' + str(method) + "_" + str(label_ratio_),
                                                      'pretrain_leaf_dnn')))
    dnn_model.to(device)

    X_emb = ae_model(torch.FloatTensor(test_X).to(device))[1]
    Y = torch.softmax(dnn_model(X_emb), dim=-1)
    Y_ = Y.cpu().detach().numpy()
    test_Y_pred = np.zeros(test_X.shape[0])

    for j in range(len(test_Y_pred)):
        y_ = Y_[j]
        # if soft_label_mapping[cluster_assignment[j]] != cat_dict['Normal']:
        #     y_[int(cat_dict['Normal'])] = 0
        y_pred = np.argmax(y_)
        test_Y_pred[j] = y_pred

    if test_Y is not None:
        print(confusion_matrix(test_Y, test_Y_pred))
        print(classification_report(test_Y, test_Y_pred))

    return test_Y_pred


def train_model():
    if not os.path.exists('models'):
        os.mkdir('models')

    if not os.path.exists(os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio))):
        os.mkdir(os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio)))
    tree_work(load_cluster_from_file=True)
    train_ae(save_path=os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio),
                                    'train_ae'), epochs=ae_epoch, load_from_file=False)
    pretrain_leaf_dnn(save_path=os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio),
                                             'pretrain_leaf_dnn'), epochs=pretrain_epoch)


# train_model()
# generate_result(test_X=test_dataset.get_x(), test_Y=test_dataset.get_y(), label_ratio_=label_ratio)
