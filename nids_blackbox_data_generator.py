import numpy as np
import pandas as pd
import torch
from torch.utils.data import Dataset

debug = False


def preprocess_dataframe(original_dataframe, cat_dict=None):
    original_dataframe["class"] = original_dataframe["class"].astype('category')

    if cat_dict is None:
        cat_dict_r = dict(enumerate(original_dataframe["class"].cat.categories))
        cat_dict_ = dict()

        for key, value in cat_dict_r.items():
            cat_dict_[value] = key
    else:
        cat_dict_ = cat_dict

    original_dataframe["class"] = original_dataframe["class"].map(cat_dict_)
    original_dataframe["class"] = original_dataframe["class"].astype('int64')

    train_np = original_dataframe.values

    trainx, trainy = train_np[:, :-1], train_np[:, -1]
    trainy = np.array(trainy).astype(int)

    return trainx, trainy, cat_dict_


class dataset_train(Dataset):

    def __init__(self, data, cat_dict):
        self.x = data[0]
        self.y = data[1]
        self.cat_dict = cat_dict

    def set_x(self, new_x):
        self.x = new_x

    def get_x(self):
        return self.x

    def get_cat_dict(self):
        return self.cat_dict

    def set_y(self, new_y):
        self.y = new_y

    def get_y(self):
        return self.y

    def get_feature_shape(self):
        return self.x.shape[1]

    def __len__(self):
        return len(self.y)

    def __getitem__(self, idx):

        return torch.from_numpy(np.array(self.x[idx])), torch.LongTensor(np.array(self.y))[idx], \
               torch.LongTensor([idx]).squeeze()

    def get_weight(self):

        trYunique, trYcounts = np.unique(self.y, return_counts=True)

        max_weight = np.max(trYcounts) / np.min(trYcounts)

        max_count = 0
        for i in range(len(trYcounts)):
            if trYunique[i] != -1 and trYcounts[i] > max_count:
                max_count = trYcounts[i]

        labels = list(self.cat_dict.values())
        no_labels = len(labels)
        weights = np.ones(no_labels)
        for i in range(len(trYunique)):
            if trYunique[i] >= 0 and trYcounts[i] > 0:
                weights[int(trYunique[i])] = min(max_weight, max_count / trYcounts[i])

        return weights

    def add_sample(self, sample_X, sample_Y):
        if len(sample_X) == 1:
            self.x = np.concatenate([self.x, np.expand_dims(sample_X, axis=0)], axis=0)
            self.y = np.append(self.y, sample_Y)
        else:
            self.x = np.concatenate([self.x, sample_X], axis=0)
            self.y = np.concatenate([self.y, sample_Y], axis=0)

    def filter(self, given_X, given_Y):
        new_lx = []
        new_ly = []
        for i in range(len(self.x)):
            if given_Y[i] >= 0:
                new_lx.append(given_X[i])
                new_ly.append(given_Y[i])

        self.x = np.array(new_lx)
        self.y = np.array(new_ly)


class dataset_test(Dataset):

    def __init__(self, cat_dict, file_path='dataset/KDDTest+.csv'):
        test = pd.read_csv(file_path)
        self.x, self.y, self.cat_dict = preprocess_dataframe(test, cat_dict)
        self.feature_size = self.x.shape[1]

    def __len__(self):
        return self.x.shape[0]

    def set_x(self, new_x):
        self.x = new_x

    def get_x(self):
        return self.x

    def get_y(self):
        return self.y

    def __getitem__(self, idx):
        return torch.from_numpy(np.array(self.x[idx])), torch.LongTensor(np.array(self.y))[idx], \
               torch.LongTensor([idx]).squeeze()


def get_training_data(label_ratio, file_path='dataset/KDDTrain+.csv'):
    train = pd.read_csv(file_path)
    train_X, train_Y, cat_dict = preprocess_dataframe(train)

    trYunique, trYcounts = np.unique(train_Y, return_counts=True)
    got_once = np.zeros(len(trYunique))

    for i in range(len(train_Y)):
        p = np.random.rand()
        if p > label_ratio and got_once[int(train_Y[i])] == 1:
            train_Y[i] = -1
        else:
            got_once[int(train_Y[i])] = 1

    labeled_data_X = []
    labeled_data_Y = []
    unlabeled_data_X = []
    unlabeled_data_Y = []

    for i in range(len(train_Y)):
        if train_Y[i] != -1:
            labeled_data_X.append(list(train_X[i]))
            labeled_data_Y.append(train_Y[i])
        else:
            unlabeled_data_X.append(list(train_X[i]))
            unlabeled_data_Y.append(train_Y[i])

    labeled_data = np.array(labeled_data_X), np.array(labeled_data_Y)
    unlabeled_data = np.array(unlabeled_data_X), np.array(unlabeled_data_Y)

    if len(unlabeled_data[1]) != 0 and len(labeled_data[1]) != 0:
        total_data_X = np.append(labeled_data[0], unlabeled_data[0], axis=0)
        total_data_Y = np.append(labeled_data[1], unlabeled_data[1], axis=0)
    elif len(unlabeled_data[1]) == 0:
        total_data_X = labeled_data[0]
        total_data_Y = labeled_data[1]
    else:
        total_data_X = unlabeled_data[0]
        total_data_Y = unlabeled_data[1]

    total_data = total_data_X, total_data_Y

    total_dataset = dataset_train(total_data, cat_dict)
    labeled_dataset = dataset_train(labeled_data, cat_dict)
    unlabeled_dataset = dataset_train(unlabeled_data, cat_dict)

    return total_dataset, labeled_dataset, unlabeled_dataset
