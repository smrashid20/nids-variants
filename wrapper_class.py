import torch
from sklearn.metrics import confusion_matrix, classification_report

from variant_dt_dnn import generate_result
from variant_dt_dnn import cat_dict
from variant_dt_dnn import test_dataset
import numpy as np

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


class BlackBoxWrapper:
    def __init__(self, input_dim, output_dim, label_ratio=0.1, path_to_model='models'):
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.label_ratio = label_ratio
        self.path_to_model = path_to_model

    def __call__(self, x):
        x = x.cpu().detach().numpy()
        y = self.getBinPrediction(x)
        y = torch.FloatTensor(y).to(device)
        return y

    def eval(self):

        return

    def getBinPrediction(self, x):
        pred_Y = generate_result(x, label_ratio_=self.label_ratio, path_to_model=self.path_to_model)
        bin_pred_y = []
        for y in pred_Y:
            if y == cat_dict["Normal"]:
                bin_pred_y.append(0)
            else:
                bin_pred_y.append(1)
        return np.array(bin_pred_y)


#
# ids_model = BlackBoxWrapper(122, 1, label_ratio=0.1, path_to_model='models')
#
# test_Y_pred = ids_model(torch.FloatTensor(test_dataset.get_x()).to(device))
# test_Y_pred = test_Y_pred.cpu().detach().numpy()
# y_true = np.zeros(test_dataset.get_y().shape)
# for i in range(len(y_true)):
#     if test_dataset.get_y()[i] == cat_dict["Normal"]:
#         y_true[i] = 0
#     else:
#         y_true[i] = 1
#
# print(confusion_matrix(y_true, test_Y_pred))
# print(classification_report(y_true, test_Y_pred))
