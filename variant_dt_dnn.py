import glob
import os
import pickle

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import random
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.tree import DecisionTreeClassifier
from torch.nn import Linear
from torch.utils.data import DataLoader

from nids_blackbox_data_generator import get_training_data, dataset_test, dataset_train

debug = False
method = "dt_dnn"
label_ratio = 0.1

np.random.seed(12345)
torch.manual_seed(12345)

random.seed(12345)

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

total_dataset, labeled_dataset, unlabeled_dataset = get_training_data(label_ratio=label_ratio)
cat_dict = total_dataset.get_cat_dict()
print(cat_dict)
test_dataset = dataset_test(cat_dict)

ae_epoch = 80
pretrain_epoch = 200
train_epoch = 200

num_data = total_dataset.get_x()
labels = total_dataset.get_y()

total_original_label_counts = dict()
distinct_labels, distinct_label_counts = np.unique(labels, return_counts=True)

for i in range(len(distinct_labels)):
    if distinct_labels[i] != -1:
        total_original_label_counts[distinct_labels[i]] = distinct_label_counts[i]

'''
undercomplete autoencoder for embedding.
'''


class AE(nn.Module):

    def __init__(self, n_input, n_z):
        super(AE, self).__init__()
        # encoder
        self.enc_1 = Linear(n_input, 96)
        self.enc_2 = Linear(96, 64)
        self.z_layer = Linear(64, n_z)

        # decoder
        self.dec_1 = Linear(n_z, 64)
        self.dec_2 = Linear(64, 96)
        self.x_bar_layer = Linear(96, n_input)

    def forward(self, x):
        # encoder
        enc_h1 = F.relu(self.enc_1(x))
        enc_h2 = F.relu(self.enc_2(enc_h1))
        z = self.z_layer(enc_h2)

        # decoder
        dec_h1 = F.relu(self.dec_1(z))
        dec_h2 = F.relu(self.dec_2(dec_h1))
        x_bar = self.x_bar_layer(dec_h2)

        return x_bar, z


class leaf_dnn(nn.Module):

    def __init__(self, n_input, n_output):
        super(leaf_dnn, self).__init__()
        self.fc1 = nn.Linear(n_input, 16)
        self.fc2 = nn.Linear(16, 8)
        self.fc3 = nn.Linear(8, n_output)

    def forward(self, x):
        out_1 = torch.relu(self.fc1(x))
        out_2 = torch.relu(self.fc2(out_1))
        out_3 = self.fc3(out_2)

        return out_3


def tree_work():
    dt_X = labeled_dataset.get_x()
    dt_Y = labeled_dataset.get_y()

    print("labeled members dimensions:")
    print(dt_X.shape)
    print(dt_Y.shape)

    clf = DecisionTreeClassifier(random_state=0, min_samples_leaf=int(len(dt_X) / 30))
    clf.fit(dt_X, dt_Y)

    print("No. of leaves of decision tree:")
    print(clf.get_n_leaves())

    '''
    saving decision tree, cluster membership info (this is just to skip the clustering step for our faster use) and soft
    labeling info.
    '''

    file = open(os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio), 'tree.pkl'), 'wb')
    pickle.dump(clf, file)
    file.close()

    leaf_dataset_X_ = dict()
    leaf_dataset_Y_ = dict()

    '''
    finding out corresponding leaf for each training sample.
    '''

    for j in range(len(num_data)):
        leaf = clf.apply([num_data[j]])[0]
        if labels[j] != -1:
            leaf_dataset_X_.setdefault(leaf, []).append(num_data[j])
            leaf_dataset_Y_.setdefault(leaf, []).append(labels[j])

    for k, v in leaf_dataset_X_.items():
        leaf_dataset_X_[k] = np.array(leaf_dataset_X_[k])
        leaf_dataset_Y_[k] = np.array(leaf_dataset_Y_[k])

    return leaf_dataset_X_, leaf_dataset_Y_


def train_ae(epochs, load_from_file=False,
             save_path=os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio), 'train_ae')):
    '''
    train autoencoder
    '''

    model = AE(total_dataset.get_feature_shape(), 32)
    model.to(device)

    if load_from_file:
        model.load_state_dict(torch.load(save_path))

    else:
        ae_train_ds = total_dataset
        training_data_length = int(0.7 * ae_train_ds.__len__())
        validation_data_length = ae_train_ds.__len__() - training_data_length
        training_data, validation_data = torch.utils.data.random_split(ae_train_ds,
                                                                       [training_data_length, validation_data_length])

        train_loader = DataLoader(training_data, batch_size=32, shuffle=True)
        validation_loader = DataLoader(validation_data, batch_size=32, shuffle=True)

        optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
        min_val_loss = 1000000

        for epoch in range(epochs):
            training_loss = 0.
            validation_loss = 0.
            train_batch_num = 0
            val_batch_num = 0

            model.train()
            for batch_idx, (x, _, idx) in enumerate(train_loader):
                x = x.float()
                x = x.to(device)

                train_batch_num = batch_idx

                optimizer.zero_grad()
                x_bar, z = model(x)
                loss = F.mse_loss(x_bar, x)
                training_loss += loss.item()

                loss.backward()
                optimizer.step()

            training_loss /= (train_batch_num + 1)

            model.eval()

            for batch_idx, (x, _, idx) in enumerate(validation_loader):
                x = x.float()
                x = x.to(device)

                val_batch_num = batch_idx

                x_bar, z = model(x)
                loss = F.mse_loss(x_bar, x)
                validation_loss += loss.item()

            validation_loss /= (val_batch_num + 1)

            if epoch % 1 == 0:
                print(
                    "epoch {} , Training loss={:.4f}, Validation loss={:.4f}".format(epoch, training_loss,
                                                                                     validation_loss))

            if epoch == 0 or min_val_loss > validation_loss:
                min_val_loss = validation_loss
                torch.save(model.state_dict(), save_path)

        print("model saved to {}.".format(save_path))
    return model


'''
pretraining using labeled and soft labeled dataset.
'''


def pretrain_leaf_dnn(save_path, epochs):
    ae_model = AE(total_dataset.get_feature_shape(), 32)
    ae_model.load_state_dict(
        torch.load(os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio), 'train_ae')))
    ae_model.to(device)

    model = leaf_dnn(32, int(max(labels)) + 1)
    model.to(device)

    weights = torch.FloatTensor(labeled_dataset.get_weight()).to(device)

    train_loader = DataLoader(labeled_dataset, batch_size=32, shuffle=True)  # soft label must be assigned

    optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
    min_train_loss = 1000000

    for epoch in range(epochs):
        train_loss = 0.0
        train_batch_num = 0
        train_num_correct = 0
        train_num_examples = 0

        model.train()
        for batch_idx, (x, y_t, idx) in enumerate(train_loader):
            x = x.float()
            x = x.to(device)
            train_batch_num = batch_idx

            optimizer.zero_grad()

            x_emb = ae_model(x)[1]
            y_pred = model(x_emb)

            y_t = y_t.clone().detach().to(device)

            loss = torch.nn.CrossEntropyLoss(weight=weights)(y_pred, y_t)
            train_loss += loss.item()

            loss.backward()
            optimizer.step()

            correct = torch.eq(torch.max(torch.softmax(y_pred, dim=-1), dim=1)[1], y_t).view(-1)
            train_num_correct += torch.sum(correct).item()
            train_num_examples += correct.shape[0]

        train_loss /= (train_batch_num + 1)
        train_acc = train_num_correct / train_num_examples

        if epoch % 1 == 0:
            print("epoch {}; T loss={:.4f} T Accuracy={:.4f}".
                  format(epoch, train_loss, train_num_correct / train_num_examples))

        if epoch == 0 or min_train_loss > train_loss:
            min_train_loss = train_loss
            torch.save(model.state_dict(), save_path)

    print("model saved to {}.".format(save_path))

    return model


'''
The dictionary of X-Y training dataset for each individual leaf.
'''
'''
training dataset of an individual leaf.
'''


def train_leaf_dnn(model, dataset, save_path, epochs):
    ae_model = AE(total_dataset.get_feature_shape(), 32)
    ae_model.load_state_dict(
        torch.load(os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio), 'train_ae')))
    ae_model.to(device)

    weights = torch.FloatTensor(dataset.get_weight()).to(device)

    train_loader = DataLoader(dataset, batch_size=32, shuffle=True)  # soft label must be assigned

    optimizer = torch.optim.Adam(model.parameters(), lr=0.0005)
    min_train_loss = 1000000
    prev_train_acc = 0
    stop_flag = 1

    for epoch in range(epochs):
        train_loss = 0.0
        train_batch_num = 0
        train_num_correct = 0
        train_num_examples = 0

        model.train()
        for batch_idx, (x, y_t, idx) in enumerate(train_loader):
            x = x.float()
            x = x.to(device)
            train_batch_num = batch_idx

            optimizer.zero_grad()

            x_emb = ae_model(x)[1]
            y_pred = model(x_emb)

            y_t = y_t.clone().detach().to(device)

            loss = torch.nn.CrossEntropyLoss(weight=weights)(y_pred, y_t)
            train_loss += loss.item()

            loss.backward()
            optimizer.step()

            correct = torch.eq(torch.max(torch.softmax(y_pred, dim=-1), dim=1)[1], y_t).view(-1)
            train_num_correct += torch.sum(correct).item()
            train_num_examples += correct.shape[0]

        train_loss /= (train_batch_num + 1)
        train_acc = train_num_correct / train_num_examples

        if epoch % 1 == 0:
            print("epoch {}; T loss={:.4f} T Accuracy={:.4f}".
                  format(epoch, train_loss, train_num_correct / train_num_examples))

        if epoch == 0 or min_train_loss > train_loss:
            min_train_loss = train_loss
            torch.save(model.state_dict(), save_path)

        if train_acc == 1.0:
            break

    print("model saved to {}.".format(save_path))

    return model


'''
training all leaves.
'''


def create_leaf_dnns(leaf_dataset_X, leaf_dataset_Y, remove_priors=True, epochs=150):
    if not os.path.exists(os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio), 'leaf_models')):
        os.mkdir(os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio), 'leaf_models'))

    if remove_priors:
        filelist = glob.glob(
            os.path.join(os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio), 'leaf_models'), "*"))
        for f in filelist:
            os.remove(f)

    for key in leaf_dataset_Y.keys():
        dataset_X = leaf_dataset_X[key]
        dataset_Y = leaf_dataset_Y[key]

        # print(key)
        # print(dataset_X.shape)
        # print(dataset_Y.shape)
        # print("\n")

        data = dataset_X, dataset_Y
        dataset = dataset_train(data, cat_dict)

        save_path = os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio), 'leaf_models',
                                 'leaf_' + str(key))

        model = leaf_dnn(32, int(max(labels)) + 1)
        model.load_state_dict(torch.load(os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio),
                                                      'pretrain_leaf_dnn')))
        model.to(device)

        train_leaf_dnn(model, dataset, save_path, epochs)


def generate_result(test_X, label_ratio_, test_Y=None, path_to_model='models'):
    clf = pickle.load(
        file=open(os.path.join(path_to_model, 'models_' + str(method) + "_" + str(label_ratio_), 'tree.pkl'), 'rb'))

    # print("Determining Leaf nodes...")
    leaf_nodes = clf.apply(test_X)

    ae_model = AE(total_dataset.get_feature_shape(), 32)
    ae_model.load_state_dict(
        torch.load(os.path.join(path_to_model, 'models_' + str(method) + "_" + str(label_ratio_), 'train_ae')))
    ae_model.to(device)
    '''
    If model does not exist for a particular leaf, use the default pretrained model.

    os.path.join(path_to_model, 'models_' + str(method) + "_" + str(label_ratio), 'leaf_models', 'leaf_' + spl[1])

    '''
    model_dict = dict()
    leaf_model_files = os.listdir(
        os.path.join(path_to_model, 'models_' + str(method) + "_" + str(label_ratio_), 'leaf_models'))
    for file in leaf_model_files:
        spl = str(file).split("_")
        leaf_model = leaf_dnn(32, int(max(labels)) + 1)
        if not os.path.exists(os.path.join(path_to_model, 'models_' + str(method) + "_" + str(label_ratio_),
                                           'leaf_models', 'leaf_' + spl[1])):
            leaf_model.load_state_dict(
                torch.load(os.path.join(path_to_model, 'models_' + str(method) + "_" + str(label_ratio_),
                                        'pretrain_leaf_dnn')))
        else:
            leaf_model.load_state_dict(
                torch.load(os.path.join(path_to_model, 'models_' + str(method) + "_" + str(label_ratio_),
                                        'leaf_models', 'leaf_' + spl[1])))
        leaf_model.to(device)
        model_dict[int(spl[1])] = leaf_model

    leaf_pred_dict = dict()
    for k, v in model_dict.items():
        leaf_model = v
        X_emb = ae_model(torch.FloatTensor(test_X).to(device))[1]
        Y = torch.softmax(leaf_model(X_emb), dim=-1)
        Y_ = Y.cpu().detach().numpy()
        leaf_pred_dict[k] = Y_

    test_Y_pred = np.zeros(test_X.shape[0])

    for j in range(len(leaf_nodes)):
        y_ = leaf_pred_dict[leaf_nodes[j]][j]
        y_pred = np.argmax(y_)
        test_Y_pred[j] = y_pred

    if test_Y is not None:
        print(confusion_matrix(test_Y, test_Y_pred))
        print(classification_report(test_Y, test_Y_pred))

    return test_Y_pred


def train_model():
    if not os.path.exists('models'):
        os.mkdir('models')

    if not os.path.exists(os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio))):
        os.mkdir(os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio)))

    leaf_dataset_X, leaf_dataset_Y = tree_work()
    train_ae(save_path=os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio),
                                    'train_ae'), epochs=ae_epoch, load_from_file=True)
    # pretrain_leaf_dnn(save_path=os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio),
    #                                          'pretrain_leaf_dnn'), epochs=pretrain_epoch)
    create_leaf_dnns(leaf_dataset_X=leaf_dataset_X, leaf_dataset_Y=leaf_dataset_Y, remove_priors=True,
                     epochs=train_epoch)

#
# train_model()
# generate_result(test_X=total_dataset.get_x(), test_Y=total_dataset.get_y(), label_ratio_=label_ratio)
# generate_result(test_X=test_dataset.get_x(), test_Y=test_dataset.get_y(), label_ratio_=label_ratio)
